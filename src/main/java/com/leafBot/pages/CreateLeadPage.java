package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.leafBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations 

	{
	@FindBy(how= How.ID,using="createLeadForm_companyName")WebElement companyname;
	
	
	public CreateLeadPage typeCompanyName(String cName) {
		
		clearAndType(companyname, cName);
		/*driver.findElementById("createLeadForm_companyName").sendKeys(cName);*/
		return this;
	}
	
	
	public CreateLeadPage typeFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		return this;
	}
	
	public CreateLeadPage typeLastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		return this;
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
