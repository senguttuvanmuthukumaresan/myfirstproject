package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;


public class ViewLeadPage extends Annotations {

		public ViewLeadPage verifyFirstName() 
	
	{
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals("FIS")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
		
	}

		public String verifyleadid()
		
		{
			WebElement leadid = driver.findElementById("viewLead_companyName_sp");
			String id1 = leadid.getText();
			String textofid = id1.replaceAll("\\D", "");
			System.out.println(textofid);
			return textofid;
		}		
		
	public FindLeadsPage clickdeletelead()
	
	{
		driver.findElementByLinkText("Delete").click();
		return new FindLeadsPage();
		
	}
		
	
}
