package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickLeadsTab() {
		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}

}
