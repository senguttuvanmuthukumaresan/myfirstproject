package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations

{
	
	
	
	public FindLeadsPage clickfindleadmenu()
	
	{
	driver.findElementByLinkText("Find Leads").click();
	return this;
	}
	
	public FindLeadsPage typeid(String text)
	
	{
	driver.findElementByName("id").sendKeys(text);
	return this;
	}
	
	public FindLeadsPage clickfindleadbutton()
	
	{
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) 
	
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return this;
	}
	

	
}

